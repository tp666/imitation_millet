var eventTester = function(data-src, m, fun){
	if(window.addEventListener){
		m.addEventListener(data-src, function(){
			if(fun){
				fun(this);
			}
		}, false)
	}else{
		m.attachEvent('on' + data-src, function(){
			if(fun){
				fun(this);
			}
		});
	}
}
