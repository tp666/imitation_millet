(function($) {
	$.extend({
		shopcar: {}
	})
	$.extend($.shopcar, {
		goodsList: [],
		init: function() {
			this.loadcookie()
			this.clickevent()
			this.jisuan()
			this.location()

		},
		location: function() {
			$(".iskong-right-bottom-right").click(function() {
				location.href = "shapping.html"
			})
            if($.cookie("username")){
                $(".denglu").text($.cookie("username"))
                $(".tuichu").text("退出")
                $(".tuichu").click(function () {
                    $.cookie("username","",{ expires: -1 })
                    $(".denglu").text("登录")
                    $(".tuichu").text("注册")
                    $(".denglu,.tuichu").click(function () {
                        location.href="login.html"
                    })
                })
            }else{
                $(".denglu").text("登录")
                $(".denglu,.tuichu").click(function () {
                    location.href="login.html"
                })
            }
		},
		jisuan: function() {
			var zongjia = 0
			var shu2 = 0
			var shu1 = 0
			$("input[type=checkbox]:gt(0):checked").each(function() {
				zongjia += parseInt($(this).parent().siblings(".zonghe").text())
				shu2 += parseInt($(this).parent().siblings(".a1").children().children(".count").text())
			})
			$("input[type=checkbox]:gt(0)").each(function() {
				shu1 += parseInt($(this).parent().siblings(".a1").children().children(".count").text())
			})
			$(".zongjia").text(zongjia)
			$(".lump-shu2").text(shu2)
			$(".lump-shu1").text(shu1)
			$.cookie("count", shu1)
		},
		zongshu: function() {
			var shu1 = 0
			for(var i = 0; i < $.shopcar.goodsList.length; i++) {
				shu1 += $.shopcar.goodsList[i].count
			}
			$(".lump-shu1").text(shu1)
		},
		clickevent: function() {
			$(".logo_").click(function() {
				location.href = "index.html"
			})

            $(".shopcar-box-goods").on("click",".lump-right",function () {
                if($.cookie("username")){
                    location.href="order.html"
                }else{
                    layer.open({
                        title: '我是提示框  ^0^',
                        type: 1,
                        area: ['420px', '240px'], //宽高
                        content: $("#zitib"),
                        btnAlign: 'c',
                        anim: 5,
                        btn: ['确定'],
						yes:function (index) {
							 location.href="login.html"
                        	layer.close(index)
                        }
                    });
                }
            })





			$("input[type=checkbox].selectall").change(function() {
				if($(this).prop("checked")) {
					$(".lump-right").css({
						background: "#ff6700",
						color: "#FFFFFF"

					})

				} else {
					$(".lump-right").css({
						background: "#e0e0e0",
						color: "#b0b0b0"

					})
				}
				$("input[type=checkbox]:gt(0)").prop("checked", $(this).prop("checked"))

			})

			$("#list").on("change", "input[type=checkbox]:gt(0)", function() {
				var istrue = true
				var i = 0
				$("input[type=checkbox]:gt(0)").each(function() {
					console.log($(this).prop("checked"))
					if(!$(this).prop("checked")) {
						istrue = false
					} else {
						i++
					}
				})
				if(i === 0) {
					$(".lump-right").css({
						background: "#e0e0e0",
						color: "#b0b0b0"

					})
				} else {
					$(".lump-right").css({
						background: "#ff6700",
						color: "#FFFFFF"
					})
				}
				$("input[type=checkbox].selectall").prop("checked", istrue)

			})

			$("#list").on("change", "input[type=checkbox]", function() {
				$.shopcar.jisuan()
			})
			$("#list").on("click", ".tp-sanchu", function() {
				var thisid = $(this).data("id")
				var thisis = $(this)
				layer.open({
					type: 1,
					title: "提示",
					area: ['300px', '150px'],
					offset: ['200px'],
					content: $('#remove'), //这里content是一个普通的String
					btn: ['删除', '取消'],
					yes: function(index, layero) {
					
						$.shopcar.goodsList = $.shopcar.goodsList.filter(function(item) {
							return item.id !== thisid
						})

						$.cookie("user", JSON.stringify($.shopcar.goodsList))

						$.shopcar.jisuan()
						console.log($.shopcar.goodsList)
						thisis.parent().parent().remove()

						$.shopcar.jisuan()
						if($.shopcar.goodsList.length === 0) {
							$(".iskong").show()
							$("#list .tr-top").remove()
							$(".shopcar-box-goods .lump-box").remove()

						}
							var istrue = true
							$("input[type=checkbox]:gt(0)").each(function() {

									if(!$(this).prop("checked")) {
										istrue = false
										console.log(istrue)
									}

								})
								$("input[type=checkbox].selectall").prop("checked", istrue)

					

						layer.close(index)

					},
					btn2: function(index, layero) {
						//按钮【按钮二】的回调

						//return false 开启该代码可禁止点击该按钮关闭
					}
				})
				
				
			})
			$("#list").on("click", ".jian", function() {
				if($(this).next().text() === "1") return
				var thisid = $(this).data("id")
				for(var i = 0; i < $.shopcar.goodsList.length; i++) {
					if($.shopcar.goodsList[i].id === thisid) {
						$.shopcar.goodsList[i].count--
							$(this).next().text($.shopcar.goodsList[i].count)
						var aa = parseFloat($(this).parent().parent().siblings(".price").text())
						$(this).parent().parent().siblings(".zonghe").text($.shopcar.goodsList[i].count * aa + "元")

						break
					}

				}
				$.cookie("user", JSON.stringify($.shopcar.goodsList))
				$.shopcar.jisuan()
				$.shopcar.zongshu()
			})
			$("#list").on("click", ".jia", function() {
				var thisid = $(this).data("id")
				for(var i = 0; i < $.shopcar.goodsList.length; i++) {
					if($.shopcar.goodsList[i].id === thisid) {

				 		$.shopcar.goodsList[i].count++
							$(this).prev().text($.shopcar.goodsList[i].count)
						var aa = parseFloat($(this).parent().parent().siblings(".price").text())
						$(this).parent().parent().siblings(".zonghe").text($.shopcar.goodsList[i].count * aa + "元")

						break
					}

				}
				$.cookie("user", JSON.stringify($.shopcar.goodsList))
				$.shopcar.jisuan()
				$.shopcar.zongshu()
			})
		},
		loadcookie: function() {
			if($.cookie("user")) {
				$.shopcar.goodsList = JSON.parse($.cookie("user"))
				$(".iskong").hide()
				$("#list").append(`
				<tr class="tr-top">
						<td style="width: 150px;"><input type="checkbox" class="selectall" style="height: 20px;width: 20px;" /> 全选</td>
						<td style="width: 380px;">商品名称</td>
						<td style="width: 210px;">单价</td>
						<td style="width: 160px;">个数</td>
						<td style="width: 150px;">总价</td>
						<td style="width: 70px;">操作</td>
					</tr>
				`)
				$(".shopcar-box-goods").append(`
				<div class="lump-box">
	 				<div class="lump-left">
						<div class="lump-left-left">
							<a href="shapping.html">继续购买</a>
						</div>
						<div class="lump-left-right">
							<p>共 <i class="lump-shu1"> 0</i>件商品，已选择 <i class="lump-shu2"> 0 </i>件</p>
						</div>
					</div>
					<div class="lump-zhong">
						<div class="lump-zhong-jiage">
							<p> 合计：<i class="zongjia">0</i> 元</p>
						</div>
					</div>
					<div class="lump-right" style="cursor: pointer">
						去结算
					</div>
				</div>`)
				$.get("data/goodslist.json", function(data) {
					var zongjia = 0
					var shu1 = 0
					data.forEach(function(item) {

						$.shopcar.goodsList.forEach(function(item1) {
							if(item1.id.toString() === item.id) {
								$("#list").append(` 	
					<tr class="article">
						<td style="width: 150px;"><input type="checkbox"  style="height: 20px;width: 20px;" />
							<div style="opacity: 0;">全是</div>
						</td>
						<td style="width: 80px;height: 80px; background-image: url(${item.pic});background-size: 100% 100%;"></td>
						<td style="width: 300px;">${item.name}</td>
						<td class="price" style="width: 210px;">${item.price}元</td>
						<td class="a1" style="width: 160px;">
							<div style="width: 160px;height: 38px;border: 1px solid #e0e0e0;display: flex;justify-content: center;">
								<div class="jian" data-id="${item.id}" style="width: 40px;text-align: center;line-height: 38px;cursor: pointer;">-</div>
								<div class="count" style="width: 80px;text-align: center;line-height: 38px;">${item1.count}</div>
								<div class="jia" data-id="${item.id}" style="width: 40px;text-align: center;line-height: 38px;cursor: pointer;">+</div>
							</div>
						</td>
						<td class="zonghe" style="width: 150px;">${item1.count * item.price}<p>元</p></td>
						<td style="width: 70px;cursor: pointer;">
							<div class="tp-sanchu" data-id="${item.id}">x</div>
						</td>
					</tr>
						   	`)
								zongjia += item1.count * item.price
								shu1 += item1.count
							}

						})

					})
					$("input[type='checkbox']").prop("checked", true)
					$(".lump-right").css({
						background: "#ff6700",
						color: "#FFFFFF"

					})
					$.cookie("count", shu1)
					$(".zongjia").text(zongjia)
					$(".lump-shu1").text(shu1)
					$(".lump-shu2").text(shu1)

					if($.shopcar.goodsList.length === 0) {
						$(".iskong").show()
						$("#list .tr-top").remove()
						$(".shopcar-box-goods .lump-box").remove()

					}
				})
			}
		}

	})
})(jQuery)