(function($) {
	$.extend({
		parameter: {}
	})
	$.extend($.parameter, {

		init: function() {
			this.tabColorPhone()
		},

		tabColorPhone: function() {

			$(".yuanB").click(function() {
				$(".goldphone").show().siblings().hide()
				$(".goldwd").css("color", "#484848")
				$(".blackwd").css("color", "#c5c5c5")
				$(".bluewd").css("color", "#c5c5c5")
				$(".whitewd").css("color", "#c5c5c5")
				$(".gold").css("background", "url(img/mi8parameter/icon-2.png) no-repeat center center")
				$(".blue").css("background", "url(img/mi8parameter/icon1.png) no-repeat center center")
				$(".white").css("background", "url(img/mi8parameter/icon3.png) no-repeat center center")
				$(".black").css("background", "url(img/mi8parameter/icon4.png) no-repeat center center")
			})

			$(".yuanA").click(function() {
				$(".bluephone").show().siblings().hide()
				$(".bluewd").css("color", "#484848")
				$(".blackwd").css("color", "#c5c5c5")
				$(".whitewd").css("color", "#c5c5c5")
				$(".goldwd").css("color", "#c5c5c5")
				$(".gold").css("background", "url(img/mi8parameter/icon2.png) no-repeat center center")
				$(".blue").css("background", "url(img/mi8parameter/icon-1.png) no-repeat center center")
				$(".white").css("background", "url(img/mi8parameter/icon3.png) no-repeat center center")
				$(".black").css("background", "url(img/mi8parameter/icon4.png) no-repeat center center")				
			})
			
			
			$(".yuanC").click(function() {
				$(".whitephone").show().siblings().hide()
				$(".whitewd").css("color", "#484848")
				$(".blackwd").css("color", "#c5c5c5")
				$(".bluewd").css("color", "#c5c5c5")
				$(".goldwd").css("color", "#c5c5c5")
				$(".gold").css("background", "url(img/mi8parameter/icon2.png) no-repeat center center")
				$(".blue").css("background", "url(img/mi8parameter/icon1.png) no-repeat center center")
				$(".white").css("background", "url(img/mi8parameter/icon-3.png) no-repeat center center")
				$(".black").css("background", "url(img/mi8parameter/icon4.png) no-repeat center center")				
			})

			$(".yuanD").click(function() {
				$(".blackphone").show().siblings().hide()
				$(".blackwd").css("color", "#484848")
				$(".whitewd").css("color", "#c5c5c5")
				$(".bluewd").css("color", "#c5c5c5")
				$(".goldwd").css("color", "#c5c5c5")
				$(".gold").css("background", "url(img/mi8parameter/icon2.png) no-repeat center center")
				$(".blue").css("background", "url(img/mi8parameter/icon1.png) no-repeat center center")
				$(".white").css("background", "url(img/mi8parameter/icon3.png) no-repeat center center")
				$(".black").css("background", "url(img/mi8parameter/icon-4.png) no-repeat center center")				
			})
		},
		
		
		
		
	});
})(jQuery)