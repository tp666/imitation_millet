(function($) {
	$.extend({
		gouwu: {}
	})
	$.extend($.gouwu, {
		init: function() {
			this.lunbo()
			this.loadAddAddress()
			this.xuanze()
		},

		lunbo: function() {
			var index = 0

			function moveA() {
				timerA = setInterval(function() {
					var outIndex = index
					index++
					if(index > ($(".nr-lunbo li").length - 1)) index = 0
					$(".nr-lunbo li").eq(outIndex).animate({
						"opacity": 0
					}, 500)
					$(".nr-lunbo li").eq(index).animate({
						"opacity": 1
					}, 500);

					$(".squaregoodsblack li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
				}, 2500)
			}
			moveA()

			function goA(transIndex) {
				var outIndex = index
				index = transIndex
				if(index > ($(".nr-lunbo li").length - 1)) {
					index = 0
				}
				$(".nr-lunbo li").eq(outIndex).animate({
					"opacity": 0
				}, 500)
				$(".nr-lunbo li").eq(index).animate({
					"opacity": 1
				}, 500);

				$(".squaregoodsblack li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
			}
			$(".squaregoodsblack li").click(function() {
				clearInterval(timerA)
				goA($(".squaregoodsblack li").index(this))
				setTimeout(function() {
					moveA()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timerA)
				var transord = index + 1
				transord = transord > $(".nr-lunbo li").length - 1 ? 0 : transord;
				goA(transord)
				setTimeout(function() {
					moveA()
				}, 0)
			})
			//向左点击
			$(".prev").click(function() {
				clearInterval(timerA)
				var transord = index - 1
				transord = transord < 0 ? ($(".nr-lunbo li").length - 1) : transord;
				goA(transord)
				setTimeout(function() {
					moveA()
				}, 0)
			})
		},

		loadAddAddress: function() {
			$("#add").click(function() {
				layer.open({
					id: "add-address-layer",
					type: 1,
					title: "新增地址",
					shadeClose: true,
					area: ['420px', '240px'], //宽高
					content: $("#template"),
					btn: ['保存', '取消'],
					succes: function(layero, index) {
						$("#add-address-layer .new-address").val("")
					},
					yes: function(index, layero) {
						var val = $.trim($("#add-address-layer .new-address").val())
						if(!val) {
							layer.alert("请输入正确地址！", {
								icon: 2,
								title: "友情提示"
							})
							return;
						}
						$(".address-list").append(
							`<li><label for="third">
									<input type="radio" name="address" id="third" value="" />
									${val}
								</label></li>`)
						layer.close(index)
					}
				});
			})
		},

		xuanze: function() {

			$(".baohuall li").click(function() {
				$(this).css("border", "1px solid #ff6700").siblings().css("border", "1px solid #e0e0e0")
				$("h2").text($(".baohu").eq($(this).index()).text())
				var h2 = parseFloat($("h2").text($(".baohu").eq($(this).index()).text()))
				$(".dui").eq($(this).index()).show().siblings().hide().parent(".gouxuan").parent(".baohu1").siblings(".baohu1").children(".gouxuan").children(".dui").hide().siblings().show()

			})

		}

	});
})(jQuery)