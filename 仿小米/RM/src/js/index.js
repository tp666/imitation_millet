(function($) {
	$.extend({
		shouye: {}
	})
	$.extend($.shouye, {
		init: function() {
			this.logo()
			this.daohang()
			this.shouji()
			this.banner()
			this.shangou()
			this.jiadian()
			this.daojishi()
			this.zhineng()
			this.dapei()
			this.peijian()
			this.zhoubian()
			this.recommend()
			this.content()
			this.video()
			this.clickevent()
			this.zhiding()
			this.tpyou()
		},
        tpyou:function(){

			$(window).scroll(function () {
				 if($(".phone-box").offset().top-$(this).scrollTop()<=200){
                     $(".tp-you").show()
                 }else{
                     $(".tp-you").hide()
                 }

            })

            $(".tp-you li").click(function () {
                var index = $(this).index()
                console.log(index)
                $(this).addClass("active").siblings().removeClass("active")
                $("body,html").animate({
                    scrollTop: $(".tp-xiaomi").eq(index).offset().top - 50
                }, 1000)

            })
            setTimeout(function () {
                    $(window).scroll(function () {
                        var scrolltop = $(window).scrollTop()
                        for (var i = 0; i < $(".tp-xiaomi").length; i++) {
                            if ($(".tp-xiaomi").eq(i).offset().top <scrolltop +200) {
                                $(".tp-you li").eq(i).addClass("active").siblings().removeClass("active")
                            }

                        }
                    })
            },500)

		},
        zhiding:function(){
            $(".left-bar li:nth-child(5)").hide()
            $(window).scroll(function () {
                if($(this).scrollTop()>400){
                    $(".left-bar li:nth-child(5)").show()
                }else{
                    $(".left-bar li:nth-child(5)").hide()
                }
            })
            if(parseInt($.cookie("count"))>0){
                $(".gouwu").text("请去为你选购的品付款吧!")
            }else{
                $(".gouwu").text("购物车中还没有商品，赶紧选购吧!")
            }
        },
		clickevent:function(){
		    $(".wodeshouji").click(function () {
                location.href="mi8summary.html"
            })
            $(".wodedianshi").click(function () {
                location.href="tv1.html"
            })
			$(".index-kefu").click(function () {
				location.href="help_center.html"
            })
			$(".index-center").click(function () {
				if($.cookie("username")){
					location.href="center.html"
				}else{
                    layer.open({
                        title: '我是提示框  ^0^ ',
                        type: 1,
                        area: ['420px', '240px'], //宽高
                        content: $("#index-login"),
                        btnAlign: 'c',
                        anim: 5,
                        btn: ['确定'],
                        yes: function (index, layero) {
                           location.href="login.html"
							layer.close(index)
                        }
                    });
				}
            })
			$(".index-shopcar").click(function () {
				location.href="shopcar.html"
            })
            $(".erweima,.er-sanjiao").hide()
            $(".daohang4").click(function () {
                location.href="shopcar.html"
            })
            $(".shopcar-count").text($.cookie("count"))
			$(".left-bar li:nth-child(5)").click(function () {
                $("body,html").animate({
                    scrollTop: 0
                }, 1000)
            })
            $(".left-bar li:nth-child(1)").hover(function () {
				$(".erweima,.er-sanjiao").show()
            },function () {
                $(".erweima,.er-sanjiao").hide()
            })
            if($.cookie("username")){
                $(".denglu").text($.cookie("username"))
				$(".tuichu").text("退出")
                $(".tuichu").click(function () {
                    $.cookie("username","",{ expires: -1 })
                    $(".denglu").text("登录")
                    $(".tuichu").text("注册")
                    $(".denglu,.tuichu").click(function () {
                        location.href="login.html"
                    })
                })
            }else{
                $(".denglu").text("登录")
                $(".denglu,.tuichu").click(function () {
                    location.href="login.html"
                })
            }

		},
		logo: function() {
			$(".logoRight .it,.logoxia").hover(function() {
				$(".logoxia").css({
					"height": "227px",
					"border-top": "1px solid #e0e0e0",
					"border-bottom": "1px solid #e0e0e0"

				})
			}, function() {
				$(".logoxia").css({
					"height": "0px",
					"border": "none"

				})
			})
		},
		daohang: function() {
			$(".it").hover(function() {
				var thisid = $(this).index()
				$(".logowu>li").eq(thisid).show().siblings().hide()
			})
		},
		banner: function() {
			var index = 0
			var isture = false
			var timer
			//悬浮左边    显示右边的物品列表
			$(".banner3>li").hide()
			$(".banner2").on("mouseover", ".item", function() {
				var thisid = $(this).index()
				$(".banner3>li").eq(thisid).show().siblings().hide()
			})
			$(".banner2").on("mouseout", ".item", function() {
				$(".banner3>li").hide()
			})
			$(".banner3").on("mouseover", "li", function() {
				var thisid = $(this).index()
				$(".banner3>li").eq(thisid).show().siblings().hide()
			})
			$(".banner3").on("mouseout", "li", function() {
				$(".banner3>li").hide()

			})

			//轮播图
			var arr = ["img/小米首页/banner1.jpg", "img/小米首页/banner2.jpg", "img/小米首页/banner3.jpg", "img/小米首页/banner4.jpg", "img/小米首页/banner5.jpg"]
			arr.forEach(function(item) {
				$(".banner1").append(`<li style="background-image: url(${item});"></li>`)
				$(".yuan").append(`<li></li>`)
				$(".yuan li").first().addClass("active")
			})
			$(".yuan li").click(function() {
				clearInterval(timer)
				var thisid = $(this).index()
				index = thisid
				run()
				timer = setInterval(right, 3000)
			})
			$(".banner-gg").mouseover(function() {
				clearInterval(timer)
			})
			$(".banner-gg").mouseout(function() {
				timer = setInterval(right, 3000)
			})
			$(".banner-left").click(function() {
				left()
			})
			$(".banner-right").click(function() {
				right()
			})
			timer = setInterval(right, 3000)

			function run() {
				$(".banner1 li").eq(index).show().siblings().hide()
				$(".yuan li").eq(index).addClass("active").siblings().removeClass("active")
			}

			function right() {
				index++
				if(index === arr.length) index = 0
				run()
			}

			function left() {
				index--
				if(index === -1) index = arr.length - 1
				run()
			}

		},
		shouji: function() {
			$.get("data/导航.json", function(data) {
				data.forEach(function(item) {
					$(".banner2").append(`
    				<li class="item" data-id="${item.id}">
	    				<a href="#" >${item.name}</a>
	    			</li>	
    				`)
					$(".banner2").on("mouseover", ".item", function() {

						var thisid = $(this).index()
						var aa = $(this).data("id")
						// console.log(thisid,aa.toString())
						$.get("data/手机.json", function(data) {
							$(".banner4").empty()
							var a = 0
							data.forEach(function(item1) {
								//console.log(item1.id,aa.toString())
								if(item1.id === aa.toString()) {
									a++
									$(".banner4").eq(thisid).append(`
                       		           <li>
                             	          <div class="banner4-pic" style="background-image: url(${item1.pic});"></div>
                                          <div class="banner4-ziti">${item1.a}</div>
                                       </li>	
                       		         `)
									//console.log($(".banner4"))
								}
							})

							if(a % 4) {
								for(var i = 0; i < 4 - a % 4; i++) {
									$(".banner4").append("<li></li>")

								}
							}
						})

					})

				})
			})

		},
		//倒计时总秒数量
		daojishi: function() {
			var a = parseInt(3800)

			function timer(a) {
				window.setInterval(function() {
					var day = 0
					var hour = 0
					var minute = 0
					var second = 0
					if(a > 0) {
						day = Math.floor(a / (60 * 60 * 24))
						hour = Math.floor(a / (60 * 60)) - (day * 24)
						minute = Math.floor(a / 60) - (day * 24 * 60) - (hour * 60)
						second = Math.floor(a) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60)
					}
					if(hour <= 9) hour = '0' + hour
					if(minute <= 9) minute = '0' + minute
					if(second <= 9) second = '0' + second

					$('.hour').html(`${hour}`)
					$('.minutes').html(`${minute}`)
					$('.seconds').html(`${second}`)
					a--
				}, 1000)
			}

			$(function() {
				timer(a);
			});
		},
		shangou: function() {
			var i = 0
			$(".shangou-top-right").click(function() {

				i -= 992
				if(i < -992) i = -992
				$(".shangou-botton-right-quan").css({
					"transform": `translateX(${i}px)`,
					"transition": "1s"
				})
			})
			$(".shangou-top-left").click(function() {

				i += 992
				if(i > 0) i = 0
				$(".shangou-botton-right-quan").css({
					"transform": `translateX(${i}px)`,
					"transition": "1s"
				})
			})
		},
		//家电
		jiadian: function() {
			$(".jiadian-top-right>li").mouseenter(function() {
				$(this).addClass("active").siblings().removeClass("active")
				var thisid = $(this).index()
				$(".jiadian-botton-right1>li").eq(thisid).show().siblings().hide()
			})
		},
		//智能
		zhineng: function() {
			$(".zhineng-top-right>li").mouseenter(function() {
				$(this).addClass("active").siblings().removeClass("active")
				var thisid = $(this).index()
				$(".zhineng-botton-right1>li").eq(thisid).show().siblings().hide()
			})
		},
		//搭配
		dapei: function() {
			$(".dapei-top-right>li").mouseenter(function() {
				$(this).addClass("active").siblings().removeClass("active")
				var thisid = $(this).index()
				$(".dapei-botton-right1>li").eq(thisid).show().siblings().hide()
			})
		},
		//配件
		peijian: function() {
			$(".peijian-top-right>li").mouseenter(function() {
				$(this).addClass("active").siblings().removeClass("active")
				var thisid = $(this).index()
				$(".peijian-botton-right1>li").eq(thisid).show().siblings().hide()
			})
		},
		//周边
		zhoubian: function() {
			$(".zhoubian-top-right>li").mouseenter(function() {
				$(this).addClass("active").siblings().removeClass("active")
				var thisid = $(this).index()
				$(".zhoubian-botton-right1>li").eq(thisid).show().siblings().hide()
			})
		},
		//热评产品
		recommend: function() {
			var i = 0
			$(".recommend-top-right").click(function() {
				i -= 1226
				if(i < -2452) i = -2452
				$(".recommend-botton1").css({
					"transition": "0.5s",
					"transform": `translateX(${i}px)`
				})
			})
			$(".recommend-top-left").click(function() {
				i += 1226
				if(i > 0) i = 0
				$(".recommend-botton1").css({
					"transition": "0.5s",
					"transform": `translateX(${i}px)`
				})
			})

		},
		//内容
		content: function() {
			var i = [0, 0, 0, 0]
			var a = [0, 0, 0, 0]
			$(".content-botton1-1 .content-botton-right").click(function() {

				i[0] -= 296
				a[0]++
					if(i[0] < -592) {
						i[0] = -592
						a[0] = 2
					}

				$(".content-botton1-1 .content-botton2").css({
					"transform": `translateX(${i[0]}px)`
				})
				$(".content-botton1-1 .content-botton-yuan1").eq(a[0]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-1 .content-botton-left").click(function() {

				i[0] += 296
				a[0]--
					if(i[0] > 0) {
						i[0] = 0
						a[0] = 0
					}

				$(".content-botton1-1 .content-botton2").css({
					"transform": `translateX(${i[0]}px)`
				})
				$(".content-botton1-1 .content-botton-yuan1").eq(a[0]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-1 .content-botton-yuan1").click(function() {
				console.log($(this).index())
				i[0] = $(this).index() * -296
				$(".content-botton1-1 .content-botton2").css({
					"transform": `translateX(${i[0]}px)`
				})
				$(this).addClass("active").siblings().removeClass("active")
			})

			$(".content-botton1-2 .content-botton-right").click(function() {

				i[1] -= 296
				a[1]++
					if(i[1] < -592) {
						i[1] = -592
						a[1] = 2
					}

				$(".content-botton1-2 .content-botton2").css({
					"transform": `translateX(${i[1]}px)`
				})
				$(".content-botton1-2 .content-botton-yuan1").eq(a[1]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-2 .content-botton-left").click(function() {

				i[1] += 296
				a[1]--
					if(i[1] > 0) {
						i[1] = 0
						a[1] = 0
					}

				$(".content-botton1-2 .content-botton2").css({
					"transform": `translateX(${i[1]}px)`
				})
				$(".content-botton1-2 .content-botton-yuan1").eq(a[1]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-2 .content-botton-yuan1").click(function() {
				i[1] = $(this).index() * -296
				$(".content-botton1-2 .content-botton2").css({
					"transform": `translateX(${i[1]}px)`
				})
				$(this).addClass("active").siblings().removeClass("active")
			})

			$(".content-botton1-3 .content-botton-right").click(function() {

				i[2] -= 296
				a[2]++
					if(i[2] < -592) {
						i[2] = -592
						a[2] = 2
					}

				$(".content-botton1-3 .content-botton2").css({
					"transform": `translateX(${i[2]}px)`
				})
				$(".content-botton1-3 .content-botton-yuan1").eq(a[2]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-3 .content-botton-left").click(function() {

				i[2] += 296
				a[2]--
					if(i[2] > 0) {
						i[2] = 0
						a[2] = 0
					}

				$(".content-botton1-3 .content-botton2").css({
					"transform": `translateX(${i[2]}px)`
				})
				$(".content-botton1-3 .content-botton-yuan1").eq(a[2]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-3 .content-botton-yuan1").click(function() {
				i[2] = $(this).index() * -296
				$(".content-botton1-3 .content-botton2").css({
					"transform": `translateX(${i[2]}px)`
				})
				$(this).addClass("active").siblings().removeClass("active")
			})

			$(".content-botton1-4 .content-botton-right").click(function() {

				i[3] -= 296
				a[3]++
					if(i[3] < -592) {
						i[3] = -592
						a[3] = 2
					}

				$(".content-botton1-4 .content-botton2").css({
					"transform": `translateX(${i[3]}px)`
				})
				$(".content-botton1-4 .content-botton-yuan1").eq(a[3]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-4 .content-botton-left").click(function() {

				i[3] += 296
				a[3]--
					if(i[3] > 0) {
						i[3] = 0
						a[3] = 0
					}

				$(".content-botton1-4 .content-botton2").css({
					"transform": `translateX(${i[3]}px)`
				})
				$(".content-botton1-4 .content-botton-yuan1").eq(a[3]).addClass("active").siblings().removeClass("active")
			})
			$(".content-botton1-4 .content-botton-yuan1").click(function() {
				i[3] = $(this).index() * -296
				$(".content-botton1-4 .content-botton2").css({
					"transform": `translateX(${i[3]}px)`
				})
				$(this).addClass("active").siblings().removeClass("active")
			})

		},
		video: function() {
			$.get("data/首页video.json", function(data) {
				data.forEach(function(item) {
					$(".video-botton").append(`
	 			<li>
					<div class="video-pic" style="background-image: url(${item.pic});">
					    <div class="video-botton-bofang">
					      <i class="fa fa-play video-botton-sanjiao" aria-hidden="true"></i>
					    </div>
					</div>
					<div class="video-botton-ziti1">${item.a}</div>
					<div class="video-botton-ziti2">${item.b}</div>
				</li>	
	 			`)

				})
			})
		}

	})
})(jQuery)