(function($) {
	$.extend({
		mi8goods: {}
	})
	$.extend($.mi8goods, {
		init: function() {
			this.loadGoodsListWhite()
			this.loadGoodsListGold()
			this.loadGoodsListBlack()
			this.loadGoodsListBlue()
			this.loadGoodsTypes()
			this.loadGoodsColor()
			this.loadTimeOver()
			this.loadAllPrice()
			this.loadAddAddress()
		},

		loadGoodsListWhite: function() {
			var index = 0

			function move() {
				timer = setInterval(function() {
					var outIndex = index
					index++
					if(index > ($(".goodsleftwhite li").length - 1)) index = 0
					$(".goodsleftwhite li").eq(outIndex).animate({
						"opacity": 0
					}, 500)
					$(".goodsleftwhite li").eq(index).animate({
						"opacity": 1
					}, 500);

					$(".squaregoodswhite li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
				}, 2500)
			}
			move()

			function go(transIndex) {
				var outIndex = index
				index = transIndex
				if(index > ($(".goodsleftwhite li").length - 1)) {
					index = 0
				}
				$(".goodsleftwhite li").eq(outIndex).animate({
					"opacity": 0
				}, 500)
				$(".goodsleftwhite li").eq(index).animate({
					"opacity": 1
				}, 500);

				$(".squaregoodswhite li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
			}
			$(".squaregoodswhite li").click(function() {
				clearInterval(timer)
				go($(".squaregoodswhite li").index(this))
				setTimeout(function() {
					move()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timer)
				var transord = index + 1
				transord = transord > $(".goodsleftwhite li").length - 1 ? 0 : transord;
				go(transord)
				setTimeout(function() {
					move()
				}, 0)
			})
			//向左点击
			$(".prev").click(function() {
				clearInterval(timer)
				var transord = index - 1
				transord = transord < 0 ? ($(".goodsleftwhite li").length - 1) : transord;
				go(transord)
				setTimeout(function() {
					move()
				}, 0)
			})
		},

		loadGoodsListBlack: function() {
			var index = 0

			function moveA() {
				timerA = setInterval(function() {
					var outIndex = index
					index++
					if(index > ($(".goodsleftblack li").length - 1)) index = 0
					$(".goodsleftblack li").eq(outIndex).animate({
						"opacity": 0
					}, 500)
					$(".goodsleftblack li").eq(index).animate({
						"opacity": 1
					}, 500);

					$(".squaregoodsblack li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
				}, 2500)
			}
			moveA()

			function goA(transIndex) {
				var outIndex = index
				index = transIndex
				if(index > ($(".goodsleftblack li").length - 1)) {
					index = 0
				}
				$(".goodsleftblack li").eq(outIndex).animate({
					"opacity": 0
				}, 500)
				$(".goodsleftblack li").eq(index).animate({
					"opacity": 1
				}, 500);

				$(".squaregoodsblack li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
			}
			$(".squaregoodsblack li").click(function() {
				clearInterval(timerA)
				goA($(".squaregoodsblack li").index(this))
				setTimeout(function() {
					moveA()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timerA)
				var transord = index + 1
				transord = transord > $(".goodsleftblack li").length - 1 ? 0 : transord;
				goA(transord)
				setTimeout(function() {
					moveA()
				}, 0)
			})
			//向左点击
			$(".prev").click(function() {
				clearInterval(timerA)
				var transord = index - 1
				transord = transord < 0 ? ($(".goodsleftblack li").length - 1) : transord;
				goA(transord)
				setTimeout(function() {
					moveA()
				}, 0)
			})
		},

		loadGoodsListBlue: function() {
			var index = 0
			function moveB() {
				timerB = setInterval(function() {
					var outIndex = index
					index++
					if(index > ($(".goodsleftblue li").length - 1)) index = 0
					$(".goodsleftblue li").eq(outIndex).animate({
						"opacity": 0
					}, 500)
					$(".goodsleftblue li").eq(index).animate({
						"opacity": 1
					}, 500);

					$(".squaregoodsblue li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
				}, 2500)
			}
			moveB()

			function goB(transIndex) {
				var outIndex = index
				index = transIndex
				if(index > ($(".goodsleftblue li").length - 1)) {
					index = 0
				}
				$(".goodsleftblue li").eq(outIndex).animate({
					"opacity": 0
				}, 500)
				$(".goodsleftblue li").eq(index).animate({
					"opacity": 1
				}, 500);

				$(".squaregoodsblue li").eq(index).addClass("activegoods").siblings().removeClass("activegoods")
			}
			$(".squaregoodsblue li").click(function() {
				clearInterval(timerB)
				goB($(".squaregoodsblue li").index(this))
				setTimeout(function() {
					moveB()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timerB)
				var transord = index + 1
				transord = transord > $(".goodsleftblue li").length - 1 ? 0 : transord;
				goB(transord)
				setTimeout(function() {
					moveB()
				}, 0)
			})
			//向左点击
			$(".prev").click(function() {
				clearInterval(timerB)
				var transord = index - 1
				transord = transord < 0 ? ($(".goodsleftblue li").length - 1) : transord;
				goB(transord)
				setTimeout(function() {
					moveB()
				}, 0)
			})
		},

		loadGoodsListGold: function() {
			var indexC = 0

			function moveC() {
				timerC = setInterval(function() {
					var outIndexC = indexC
					indexC++
					if(indexC > ($(".goodsleftgold li").length - 1)) indexC = 0
					$(".goodsleftgold li").eq(outIndexC).animate({
						"opacity": 0
					}, 500)
					$(".goodsleftgold li").eq(indexC).animate({
						"opacity": 1
					}, 500);

					$(".squaregoodsgold li").eq(indexC).addClass("activegoods").siblings().removeClass("activegoods")
				}, 2500)
			}
			moveC()

			function goC(transIndexC) {
				var outIndexC = indexC
				indexC = transIndexC
				if(indexC > ($(".goodsleftgold li").length - 1)) {
					indexC = 0
				}
				$(".goodsleftgold li").eq(outIndexC).animate({
					"opacity": 0
				}, 500)
				$(".goodsleftgold li").eq(indexC).animate({
					"opacity": 1
				}, 500);

				$(".squaregoodsgold li").eq(indexC).addClass("activegoods").siblings().removeClass("activegoods")
			}
			$(".squaregoodsgold li").click(function() {
				clearInterval(timerC)
				goC($(".squaregoodsgold li").index(this))
				setTimeout(function() {
					moveC()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timerC)
				var transordC = indexC + 1
				transordC = transordC > $(".goodsleftgold li").length - 1 ? 0 : transordC;
				goC(transordC)
				setTimeout(function() {
					moveC()
				}, 0)
			})
			//向左点击
			$(".prev").click(function() {
				clearInterval(timerC)
				var transordC = indexC - 1
				transordC = transordC < 0 ? ($(".goodsleftgold li").length - 1) : transordC;
				goC(transordC)
				setTimeout(function() {
					moveC()
				}, 0)
			})
		},

		loadGoodsTypes: function() {
			$(".righttypes li").click(function() {
				$(this).addClass("activeboder").siblings().removeClass("activeboder")
				$(this).children(".typesprice").addClass("activeb").parent().siblings("li").children(".typesprice").removeClass("activeb")
				$(this).children(".typesname").addClass("activea").parent().siblings("li").children(".typesname").removeClass("activea")
				$(".rightprice li").eq($(this).index()).show().siblings().hide()
				$(".xinghao li").eq($(this).index()).show().siblings().hide()
				$(".jiaqian li").eq($(this).index()).show().siblings().hide()
				//手机价格
				$(".zjprice").text($(".nowprice").eq($(this).index()).text())
			})
			$(".baohuall li").click(function(){
				$(this).css("border","1px solid #ff6700").siblings().css("border","1px solid #e0e0e0")
				//保护价格				
				$(".bhprice").text($(".baohu").eq($(this).index()).text())
				$(".dui").eq($(this).index()).show().siblings().hide().parent(".gouxuan").parent(".baohu1").siblings(".baohu1").children(".gouxuan").children(".dui").hide().siblings().show()
				$(".baohujiaqian li").eq($(this).index()).show().siblings().hide()
				$(".baohumingcheng li").eq($(this).index()).show().siblings().hide()
			})
		},

		loadGoodsColor: function() {
			$(".rightcolor li").click(function() {
				$(this).addClass("activeboder").siblings().removeClass("activeboder")
				$(".yanse li").eq($(this).index()).show().siblings().hide()
				$(this).children(".yuanwhite").children().addClass("colora").parent(".yuanwhite").parent().siblings("li").children(".yuanwhite").children().removeClass("colora")
				$(".goodsleft div").eq($(this).index()).show().siblings().hide()		
				$(".goodsleftxia li").eq($(this).index()).show().siblings().hide()
				$(".next").show()
				$(".prev").show()
			})
		},

		loadTimeOver: function() {
			var time = parseInt(36000)
			function timer(time) {
				window.setInterval(function() {
					var day = 0
					var hour = 0
					var minute = 0
					var second = 0
					if(time > 0) {
						day = Math.floor(time / (60 * 60 * 24))
						hour = Math.floor(time / (60 * 60)) - (day * 24)
						minute = Math.floor(time / 60) - (day * 24 * 60) - (hour * 60)
						second = Math.floor(time) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60)
					}
					if(hour <= 9) hour = '0' + hour
					if(minute <= 9) minute = '0' + minute 
					if(second <= 9) second = '0' + second					
					$(".hour").html(`${hour}`)
					$(".minutes").html(`${minute}`)
					$(".seconds").html(`${second}`)
					time--
				},1000)
			}
			$(function(){
				timer(time)
			})
		},
		
		loadAddAddress: function(){
			$("#add").click(function(){
				layer.open({
					id: "add-address-layer",
					type: 1,
					title:"新增地址",
					shadeClose: true,
					area:['420px','240px'],//宽高
					content:$("#template"),
					btn:['保存','取消'],
					succes:function(layero,index){
						$("#add-address-layer .new-address").val("")
					},
					yes: function(index,layero){
						var val =$.trim($("#add-address-layer .new-address").val())							
							if(!val){
								layer.alert("请输入正确地址！", {icon: 2,title:"友情提示"})
								return;
							}														
							$(".address-list").append(
								`<li><label for="third">
									<input type="radio" name="address" id="third" value="" />
									${val}
								</label></li>`)
							layer.close(index)
					}					
				});
			})
		},
		
		loadAllPrice: function(){
			
		}

	});
})(jQuery)