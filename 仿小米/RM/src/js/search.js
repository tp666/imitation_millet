(function($) {
    $.extend({
        makeUrl: function(url, args) {
            if(!args) return url

            var arr = []
            for(var key in args) {
                arr.push(key + "=" + escape(args[key]))
            }

            url += ("?" + arr.join('&'))

            return url
        },
        //获取查询参数
        getSearchParam: function(key) {
            if(!location.search) return null;
            var argstr = location.search.substring(1)
            var argsarr = argstr.split('&')
            for(var i = 0; i < argsarr.length; i++) {
                var item = argsarr[i].split('=')
                if(item[0] === key) {
                    return unescape(item[1])
                }
            }
        }
    });
})(jQuery)