(function($) {
	$.extend({
		dianshi: {}
	})
	$.extend($.dianshi, {
		init: function() {
			this.dingwei()
			this.dianying1()
			this.imgPlay()
			this.qiehuan()
			this.qiehuan1()
			},
		dingwei: function() {
			$(window).scroll(function() {
				if($(document).scrollTop() > 140) {
					$(".bg").css("position", "fixed").css("top", 0)
				} else {
					$(".bg").css("position", "static")
				}
			})
		},

		qiehuan: function() {
			$(".bd-8-qiehuan-text li").click(function() {
				document.getElementsByClassName("bd-8-qiehuan-text1")[0].classList.remove("bd-8-qiehuan-text1")
				this.classList.add("bd-8-qiehuan-text1")
				$(".bd-8-qiehuan-img li").eq($(this).index()).show().siblings().hide()
			})
		},

		qiehuan1: function() {
			$(".bd-5-ul li").click(function() {
				document.getElementsByClassName("bd-5-ul1")[0].classList.remove("bd-5-ul1")
				this.classList.add("bd-5-ul1")
				$(".bd-5-imgs li").eq($(this).index()).show().siblings().hide()
			})
		},

		imgPlay: function() {
			var i = 0;
			var timer;

			function han() {
				$(".lun-wai li").eq(i).show().siblings().hide();

			}

			function start() {
				timer = setInterval(function() {
					han()
					i++
					if(i === $(".lun-wai li").length) {
						i = 0
					}

				}, 2000)
			}

			function dianji() {
				$(".lun-left").click(function() {
					i--;
					if(i < 0) {
						i = $(".lun-wai li").length - 1;
					}
					han()
					clearInterval(timer)
				})
				$(".lun-right").click(function() {
					i++;
					if(i === $(".lun-wai li").length) {
						i = 0;
					}
					han()
					clearInterval(timer)
				})
			}
			dianji()
			start()
		},

		
		dianying1: function() {
			var index = 0
			setInterval(function() {
				index++
				$(".bd-8-img1 li").eq(index).fadeIn().siblings().fadeOut()
				if(index === $(".bd-8-img1 li").length - 1)
					index = -1
			}, 1500)
		}
	});
})(jQuery)