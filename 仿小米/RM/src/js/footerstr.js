var footerstr = `  <div class="footer-box">
                    <ul class="footer-box-top">
						<li>
							<div class="footer-box-top-ziti">
								<div class="footer-box-top-pic tp1"style="transform: rotate(270deg);"></div>
								&nbsp;预约维修服务
							</div>
						</li>
						<li>
							<div class="footer-box-top-ziti">
								<div class="footer-box-top-pic tp2"></div>
								&nbsp;7天无理由退货</div>
						</li>
						<li>
							<div class="footer-box-top-ziti">
								<div class="footer-box-top-pic tp3"></div>
								&nbsp;15天免费换货
							</div>
						</li>
						<li>
							<div class="footer-box-top-ziti">
								<div class="footer-box-top-pic tp4"></div>
								&nbsp;满150元包邮
							</div>
						</li>
						<li>
							<div class="footer-box-top-ziti">
								<div class="footer-box-top-pic tp5"></div>
								&nbsp;520余家售后网点
							</div>
						</li>
                    </ul>
                    <div class="footer-box-botton">
                    	<ul class="footer-box-botton-left">
                    		<li>
                    			<dt>帮助中心</dt>
                    			<dd>账户管理</dd>
                    			<dd>购物指南</dd>
                    			<dd>订单操作</dd>
                    		</li>
                    		<li>
                    			<dt>服务支持</dt>
                    			<dd>售后政策</dd>
                    			<dd>自助服务</dd>
                    			<dd>相关下载</dd>
                    		</li>
                    		<li>
                    			<dt>线下门店</dt>
                    			<dd>小米之家</dd>
                    			<dd>服务网点</dd>
                    			<dd>授权体验店</dd>
                    		</li>
                    		<li>
                    			<dt>关于小米</dt>
                    			<dd>了解小米</dd>
                    			<dd>加入小米</dd>
                    			<dd>投资者关系</dd>
                    		</li>
                    		<li>
                    			<dt>关注我们</dt>
                    			<dd>新浪微博</dd>
                    			<dd>官方微信</dd>
                    			<dd>联系我们</dd>
                    		</li>
                    		<li style="border-right:1px solid #757575;">
                    			<dt>特色服务</dt>
                    			<dd>F 码通道</dd>
                    			<dd>礼物码</dd>
                    			<dd>防伪查询</dd>
                    		</li>
                    	</ul>
                    	<div class="footer-box-botton-right">
                    		<p class="footer-box-botton-right-dianhua">400-100-5678</p>
                    	    <p class="footer-box-botton-right-time">
                    	    	周一至周日 8:00-18:00
                    	    <br/>
                    	        （仅收市话费）
                    	    </p>
                    	    <div class="footer-box-botton-right-box">
                    	    	 联系客服
                    	    </div>
                    	</div>
                    </div>
                </div>
                <div class="footer-info-box">
                	<div class="footer-info">
                		<div class="footer-info-top">
                			<div class="footer-info-top-left">
                				<div class="footer-info-top-left-pic"style="background-image: url(img/小米首页/logo-footer.png);"></div>
                			</div>
                			<div class="footer-info-zhong">
                				<ul class="footer-info-zhong-top">
                					<li>小米商城</li>
                					<li>MlUl</li>
                					<li>米家</li>
                					<li>米聊</li>
                					<li>多看</li>
                					<li>游戏</li>
                					<li>路由器</li>
                					<li>米粉卡</li>
                					<li>政企服务</li>
                					<li>隐私政策</li>
                					<li>问题反馈</li>
                					<li>廉政举报</li>
                					<li style="border: none;">Select Region</li>
                				</ul>
                				<div class="footer-info-zhong-botton">
                					<p>© <a>mi.com</a> 京ICP证110507号 <a>京ICP备10046444号</a> <a>京公网安备11010802020134号 </a></a>京网文[2017]1530-131号<a><br />
                					<a>（京）网械平台备字（2018）第00005号</a> <a>互联网药品信息服务资格证 (京) -非经营性-2014-0090</a> <a>营业执照</a> <a>医疗器械公告</a><br />
                					违法和不良信息举报电话：185-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试
                					</p>
                				</div>
                			
                			</div>
                			<ul class="footer-info-right">
                				<li style="background-image: url(img/小米首页/info1.png);"></li>
                				<li style="background-image: url(img/小米首页/info2.png);"></li>
                				<li style="background-image: url(img/小米首页/info3.png);"></li>
                				<li style="background-image: url(img/小米首页/info4.png);"></li>
                				<li style="background-image: url(img/小米首页/info5.png);"></li>
                			</ul>
                		</div>
                		<div class="footer-info-botton-fasha">
                			探索黑科技，小米为发烧而生！
                		</div>
                	</div>
                </div>`
$("footer")[0].innerHTML = footerstr