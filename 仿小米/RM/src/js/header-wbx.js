(function($) {
    $.extend({
        shouye: {}
    })
    $.extend($.shouye, {
        init: function() {
            this.logo()
            this.daohang()
            this.shouji()
            this.banner()
            this.dingwei()
            this.clickevent()
        },
        clickevent:function(){
            $(".daohang4").click(function () {
                location.href="shopcar.html"
            })
            $(".logo_").click(function () {
                location.href="index.html"
            })
            if($.cookie("username")){
                $(".denglu").text($.cookie("username"))
                $(".tuichu").text("退出")
                $(".tuichu").click(function () {
                    $.cookie("username","",{ expires: -1 })
                    $(".denglu").text("登录")
                    $(".tuichu").text("注册")
                    $(".denglu,.tuichu").click(function () {
                        location.href="login.html"
                    })
                })
            }else{
                $(".denglu").text("登录")
                $(".denglu,.tuichu").click(function () {
                    location.href="login.html"
                })
            }

        },
        dingwei: function(){
            $(window).scroll(function(){
                if($(document).scrollTop()>140){
                    $(".bg").css("position","fixed").css("top",0)
                } else {
                    $(".bg").css("position","static")
                }
            })
        },

        logo: function() {
            $(".logoRight .it,.logoxia").hover(function() {
                $(".logoxia").css({
                    "height": "227px",
                    "border-top": "1px solid #e0e0e0",
                    "border-bottom": "1px solid #e0e0e0"
                })
            }, function() {
                $(".logoxia").css({
                    "height": "0px",
                    "border": "none"
                })
            })
        },
        daohang: function() {
            $(".it").hover(function() {
                var thisid = $(this).index()
                $(".logowu>li").eq(thisid).show().siblings().hide()
            })
        },

        banner: function() {
            var index=0
            var isture=false
            //悬浮左边    显示右边的物品列表
            $(".banner3>li").hide()
            $(".banner2").on("mouseover", ".item", function() {
                var thisid = $(this).index()
                $(".banner3>li").eq(thisid).show().siblings().hide()
            })
            $(".banner2").on("mouseout", ".item", function() {
                $(".banner3>li").hide()
            })
            $(".banner3").on("mouseover", "li", function() {
                var thisid = $(this).index()
                $(".banner3>li").eq(thisid).show().siblings().hide()
            })
            $(".banner3").on("mouseout", "li", function() {
                $(".banner3>li").hide()
            })
        },

        shouji: function() {
            $.get("data/导航.json", function(data) {
                data.forEach(function(item) {
                    $(".banner2").append(`
    				<li class="item" data-id="${item.id}">
	    				<a href="#" >${item.name}</a>
	    			</li>	
    				`)
                    $(".banner2").on("mouseover",".item",function(){
                        var thisid = $(this).index()
                        var aa = $(this).data("id")
                        // console.log(thisid,aa.toString())
                        $.get("data/mobile.json", function(data) {
                            $(".banner4").empty()
                            var a = 0
                            data.forEach(function(item1) {
                                //console.log(item1.id,aa.toString())
                                if(item1.id === aa.toString()) {
                                    a++
                                    $(".banner4").eq(thisid).append(`
                       		           <li>
                             	          <div class="banner4-pic" style="background-image: url(${item1.pic});"></div>
                                          <div class="banner4-ziti">${item1.a}</div>
                                       </li>	
                       		         `)
                                }
                            })
                            if(a % 4) {
                                for(var i = 0; i < 4 - a % 4; i++) {
                                    $(".banner4").append("<li></li>")
                                }
                            }
                        })
                    })
                })
            })
        }
    })
})(jQuery)