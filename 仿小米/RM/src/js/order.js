(function ($) {
    $.extend({
        order: {}
    })
    $.extend($.order, {
        init: function () {
            this.clickevent()
            this.loadcookie()
            if($.cookie("add")){
                $.order.add=JSON.parse($.cookie("add"))
                $.order.add.forEach(function (item) {
                    $(".first").before(`
                         <li class="order-gg">
                              <div class="order-name">${item.name}</div>
                              <div class="order-tel">${item.tel}</div>
                              <div class="order-add1">${item.add1}</div>
                              <div class="order-add2">${item.add2}</div>
                              <div class="order-xiugai">修改</div>
                        </li>
                        `)
                })
            }
        },
        goodsList:[],
        add:[],
        clickevent: function () {
            $(".address-out2").blur(function () {
                if((!/^1[34578]\d{9}$/.test($(".address-out2").val()))){
                    layui.use("layer",function () {
                        var layer = layui.layer;
                        layer.open({
                            title: "提示",
                            content: "请输入正确的手机号",
                            shadeclose: true,
                        })
                    })
                }
            })

            $(".first").click(function () {
                //页面层
                tan()

            })
            $(".order-jiesuan2").click(function () {
                var istrue=false
                 for(var i=0;i<$(".order-gg").length;i++){
                     if($(".order-gg").eq(i).hasClass("active")){
                         istrue=true
                         $.cookie("ordername",$(".order-gg").eq(i).children(".order-name").text())
                         $.cookie("ordertel",$(".order-gg").eq(i).children(".order-tel").text())
                         $.cookie("orderadd1",$(".order-gg").eq(i).children(".order-add1").text())
                         $.cookie("orderadd2",$(".order-gg").eq(i).children(".order-add2").text())
                     }
                 }
                 if(istrue){
                      location.href="jiesuan.html"

                 }else{
                     layer.open({
                         title: '我是提示框  ^0^',
                         type: 1,
                         area: ['420px', '240px'], //宽高
                         content: $("#zitia"),
                         btnAlign: 'c',
                         anim: 5,
                         btn: ['确定']
                     });
                 }
            })
            if($.cookie("username")){
                $(".denglu").text($.cookie("username"))
                $(".tuichu").text("退出")
                $(".tuichu").click(function () {
                    $.cookie("username","",{ expires: -1 })
                    $(".denglu").text("登录")
                    $(".tuichu").text("注册")
                    $(".denglu,.tuichu").click(function () {
                        location.href="login.html"
                    })
                })
            }else{
                $(".denglu").text("登录")
                $(".denglu,.tuichu").click(function () {
                    location.href="login.html"
                })
            }
            $(".order-logo_").click(function () {
                location.href="index.html"
            })
            $(".address-hang>input").focus(function () {
                var thisid = $(this).index()
                $(".address-in").eq(thisid).css("border", "1px solid #ff6700")
            })
            $(".address-hang input").blur(function () {
                var thisid = $(this).index()
                $(".address-in").eq(thisid).css("border", "1px solid #A9A9A9")
            })
            $("textarea").focus(function () {
                var thisid = $(this).index()
                $(".address-in1").eq(thisid).css("border", "1px solid #ff6700")
            })
            $("textarea").blur(function () {
                var thisid = $(this).index()
                $(".address-in1").eq(thisid).css("border", "1px solid #A9A9A9")
            })
            $(".order-dizhi").on("click",".order-gg",function () {
                $(this).addClass("active").siblings().removeClass("active")
                $(".order-add-nei1").text($(".active .order-name").text()+$(".active .order-tel").text())
                $(".order-add-nei2").text($(".active .order-add1").text()+$(".active .order-add2").text())
            })
            $(".order-dizhi").on("click","li .order-xiugai",function () {
                     var thisid=$(this).parent().index()
                     console.log($(".order-dizhi li").eq(thisid).children(".order-name").text())
                $(".address-out1").val($(".order-dizhi li").eq(thisid).children(".order-name").text())
                $(".address-out2").val($(".order-dizhi li").eq(thisid).children(".order-tel").text())
                $("#city-picker3").val($(".order-dizhi li").eq(thisid).children(".order-add1").text())
                $(".address-out3").val($(".order-dizhi li").eq(thisid).children(".order-add2").text())
                layer.open({
                    title: '添加收货地址',
                    type: 1,
                    area: ['660px', '460px'], //宽高
                    content: $("#address-box"),
                    btnAlign: 'c',
                    anim: 5,
                    btn: ['保存', '取消'],
                    yes: function (index, layero) {
                        //按钮【按钮一】的回调
                        if((!/^1[34578]\d{9}$/.test($(".address-out2").val())) || ($(".address-out1").val() === "")||$("#city-picker3").val()==="") {
                            if($(".address-out1").val() === ""){
                                layui.use("layer", function () {
                                    var layer = layui.layer;
                                    layer.open({
                                        title: "提示",
                                        content: "姓名不能为空",
                                        shadeclose: true,
                                    })
                                })
                            }else if((!/^1[34578]\d{9}$/.test($(".address-out2").val()))){
                                layui.use("layer", function () {
                                    var layer = layui.layer;
                                    layer.open({
                                        title: "提示",
                                        content: "手机号码不能为空",
                                        shadeclose: true,
                                    })
                                })
                            }else if($("#city-picker3").val()===""){
                                layui.use("layer", function () {
                                    var layer = layui.layer;
                                    layer.open({
                                        title: "提示",
                                        content: "地址不能为空",
                                        shadeclose: true,
                                    })
                                })
                            }
                        } else{
                            console.log($(".address-out1").val())
                            console.log(thisid)
                            $.order.add[thisid].name=($(".address-out1").val())
                            $.order.add[thisid].tel=($(".address-out2").val())
                            $.order.add[thisid].add1=($("#city-picker3").val())
                            $.order.add[thisid].add2=($(".address-out3").val())
                            $.cookie("add",JSON.stringify($.order.add))
                            $(".order-dizhi li").eq(thisid).children(".order-name").text($(".address-out1").val())
                            $(".order-dizhi li").eq(thisid).children(".order-tel").text($(".address-out2").val())
                            $(".order-dizhi li").eq(thisid).children(".order-add1").text($("#city-picker3").val())
                            $(".order-dizhi li").eq(thisid).children(".order-add2").text($(".address-out3").val())
                            layer.close(index)
                        }
                    },
                    btn2: function (index, layero) {
                        //按钮【按钮二】的回调

                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            })


            function tan() {
                layer.open({
                    title: '添加收货地址',
                    type: 1,
                    area: ['660px', '460px'], //宽高
                    content: $("#address-box"),
                    btnAlign: 'c',
                    anim: 5,
                    btn: ['保存', '取消'],
                    yes: function (index, layero) {
                        //按钮【按钮一】的回调
                        if((!/^1[34578]\d{9}$/.test($(".address-out2").val())) || ($(".address-out1").val() === "")||$("#city-picker3").val()==="") {
                            if($(".address-out1").val() === ""){
                                layui.use("layer", function () {
                                    var layer = layui.layer;
                                    layer.open({
                                        title: "提示",
                                        content: "姓名不能为空",
                                        shadeclose: true,
                                    })
                                })
                            }else if((!/^1[34578]\d{9}$/.test($(".address-out2").val()))){
                               layui.use("layer", function () {
                                   var layer = layui.layer;
                                   layer.open({
                                       title: "提示",
                                       content: "手机号码不能为空",
                                       shadeclose: true,
                                   })
                               })
                           }else if($("#city-picker3").val()===""){
                               layui.use("layer", function () {
                                   var layer = layui.layer;
                                   layer.open({
                                       title: "提示",
                                       content: "地址不能为空",
                                       shadeclose: true,
                                   })
                               })
                           }
                        }else{
                                $.order.add.push({
                                    "name":$(".address-out1").val(),
                                    "tel":$(".address-out2").val(),
                                    "add1":$("#city-picker3").val(),
                                    "add2":$(".address-out3").val()
                                })
                                $.cookie("add",JSON.stringify($.order.add))
                                $(".first").before(`
                         <li class="order-gg">
                              <div class="order-name">${$(".address-out1").val()}</div>
                              <div class="order-tel">${$(".address-out2").val()}</div>
                              <div class="order-add1">${$("#city-picker3").val()}</div>
                              <div class="order-add2">${$(".address-out3").val()}</div>
                              <div class="order-xiugai">修改</div>
                        </li>
                        `)

                                layer.close(index)

                        }



                    },
                    btn2: function (index, layero) {
                        //按钮【按钮二】的回调

                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });

            }


        },
        // address:function () {
        //
        // }
        loadcookie: function () {
            var count=0
            var zongshu=0
            if ($.cookie("user")) {
                $.order.goodsList = JSON.parse($.cookie("user"))
                $.get("data/goodslist.json", function (data) {

                    data.forEach(function (item) {
                        $.order.goodsList.forEach(function (item1) {
                            if (item1.id.toString() === item.id) {
                                $(".order-goodslist").append(` 
                                 <tr class="order-it">
                                      <td style="height: 30px;width: 30px;background-image: url(${item.pic});background-size: 100% 100%;"></td>
                                      <td style="height: 30px;width: 650px;line-height: 30px;color: #424242;font-size: 12px;margin-left: 10px;">
                                       ${item.name}
                                      </td>
                                      <td style="height: 30px;width: 150px;text-align: center;color: #424242;font-size: 12px;line-height: 30px;">
                                      ${item.price}元 x ${item1.count}
                                      </td>
                                      <td style="height: 30px;width: 100px;"></td>
                                      <td style="height: 30px;width: 190px;text-align: center;color: #ff6700;font-size: 12px;line-height: 30px;">
                                      ${item.price*item1.count}元
                                      </td>
                                 </tr>
					          
						   	`)
                                count+=item1.count
                                zongshu+=item.price*item1.count
                            }


                        })

                    })
                    $(".itt1").text(count)
                    $(".itt2").text(zongshu)
                    $.cookie("zhifu",zongshu)
                })
            }
        }

    })
})(jQuery)