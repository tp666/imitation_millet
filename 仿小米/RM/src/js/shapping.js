(function ($) {
    $.extend({
        shopping: {}
    })
    $.extend($.shopping, {
        shopcar:[],
        shopxihuan:[],
        init: function () {
            this.loadcategorize()
            this.loadbrand()
            this.clickevent()
            this.loadgoodslist()
            if($.cookie("user")) {
                $.shopping.shopcar = JSON.parse($.cookie("user"))
            }
            this.shopcarcount()
        },
        shopcarcount:function(){
            var a=0
            $.shopping.shopcar.forEach(function (item) {
                a+=item.count
            })
            $(".shopcar-count").text(a)


        },
        clickevent: function () {
            $(".categorize,.brand,.type").on("click", ".it", function () {
                $(this).addClass("active").siblings().removeClass("active")
                $.shopping.loadgoodslist()
            })
            $(".aa").click(function () {
                $.shopping.loadgoodslist()
            })
            $(window).keydown(function (e) {
                if(e.keyCode===13){
                    $.shopping.loadgoodslist()
                }
            })
            $(".tp-goodslist").on("click",".tianjia",function () {
                if($.shopping.shopcar.length){
                    var istrue=true
                    for(var i=0;i<$.shopping.shopcar.length;i++){
                        if($.shopping.shopcar[i].id===$(this).data("id")){
                           $.shopping.shopcar[i].count++
                            istrue=false
                            break
                        }
                    }
                    if(istrue){
                        $.shopping.shopcar.push({
                            id:$(this).data("id"),
                            count:1
                        })
                    }
                }else{
                    $.shopping.shopcar.push({
                        id:$(this).data("id"),
                        count:1
                    })
                }
                $.cookie("user",JSON.stringify($.shopping.shopcar))
                    if($(this).next().data("id")===$(this).data("id")){
                        $(this).next().css({
                            height:"50px",
                            transition:"0.5s"
                        })
                        var thisid=$(this)
                        setTimeout(function () {
                            thisid.next().css({
                                height:"0px",
                                transition:"0.5s"
                            })
                        },1000)
                    }
                    $.shopping.shopcarcount()
            })

            var isbian=false
            $(".tp-goodslist").on("click",".tp-goodslist-aixing",function () {
                if(!isbian){
                    $(this).html(`<img src="img/shapping/aixin(1).png" style="height: 23px;width: 25px;"/>

                        	<p>喜欢</p>`)
                    isbian=true
                }else{
                    $(this).html(`<img src="img/shapping/aixin_ (1).png" style="height: 25px;width: 25px;"/>
                        	<p>喜欢</p>`)
                    isbian=false
                }
            })
        },
        loadcategorize: function () {
            $.get("data/categorize.json", function (data) {
                data.forEach(function (item) {
                    $(".categorize").append(`<li class="it" data-id="${item.id}">${item.categorize}</li>`)
                })
            })

        },
        loadbrand: function () {
            $.get("data/brand.json", function (data) {
                data.forEach(function (item) {
                    $(".brand").append(`<li class="it" data-id="${item.id}">${item.brand}</li>`)
                })
            })
        },
        loadgoodslist: function () {
            var categorize = $(".categorize .it.active").data("id")
            var brand = $(".brand .it.active").data("id")
            $.get("data/goodslist.json",function (data) {
             
                var arr = []
                var acc = []
                data.forEach(function (item1) {
                    if(brand&&categorize){
                        if(item1.brand===brand.toString()&&item1.categorize===categorize.toString())
                            arr.push(item1)
                    }
                    else if(!brand&&categorize){
                        if(item1.categorize===categorize.toString())
                            arr.push(item1)
                    }
                    else if(brand&&!categorize){
                        if(item1.brand===brand.toString())
                            arr.push(item1)
                    }
                    else {
                        arr=data
                    }
                })
                arr.forEach(function (item2) {
                    if(item2.name.indexOf($(".logoshou1").val())>-1) {
                        acc.push(item2)
                    }
                })
                var pageSize = 16
                $("#pager").empty()
                   $("#pager").initPager(acc.length,pageSize, function(currindex,start,end) {
                       loadList(currindex,start,end)
                   })

                function loadList(currindex, start, end) {
                    var a=0
                    $(".tp-goodslist").empty()
                    var crr =acc.slice(start, end)	
                    crr.forEach(function (item) {
                        if(item.name.indexOf($(".logoshou1").val())>-1){
                             if(item.name.indexOf($(".logoshou1").val())>0)$("#pager").empty()
                            var kw=$(".logoshou1").val()
                            if(kw) kw="<span style='color: #ff6700;'>"+$(".logoshou1").val()+"</span>"
                            a++ 
                            $(".tp-goodslist").append(`
					<li class="it">
						<div class="tp-goodslist-pic" style="background-image: url(${item.pic})"></div>
						<div class="tp-goodslist-ziti">${item.jieshao}</div>
						<h2 class="tp-goodslist-ziti1">${item.name.replace($(".logoshou1").val(),kw)}</h2>
                        <p>${item.price}元</p>
                        <div class="tp-goodslist-aixing">
                        	<img src="img/shapping/aixin_ (1).png" style="height: 25px;width: 25px;"/>
                        	<p>喜欢</p>
                        </div>
                        <div class="tianjia" data-id="${item.id}">
                        	<p>加入购物车</p>
                        	<img src="img/shapping/icon2 (1).png" style="height: 25px;width: 25px;"  //>
                        </div>
                        <div class="join" data-id="${item.id}">成功加入购物车</div>
					</li>
					`)
                        }


                    })

                    if(a%4){
                        for(var i=0;i<4-a%4;i++){
                            $(".tp-goodslist").append("<li style='background: transparent'></li>")

                        }
                    }
                }

            })
        }

    });
})(jQuery)
