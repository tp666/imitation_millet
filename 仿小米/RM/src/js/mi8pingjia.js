(function($) {
	$.extend({
		evaluate: {}
	})
	$.extend($.evaluate, {
		init: function() {
			$.evaluate.add()
			$.evaluate.alert()
			this.tanchu()
		},
		
		tanchu: function(){
			$(".third1").click(function(){
				alert("确定提交吗？")
			})
		},
		//		加载评分
		add: function() {
			layui.use('rate', function() {
				var rate = layui.rate;

				//渲染
				var ins1 = rate.render({
					elem: '#test1', //绑定元素
					theme: '#ff6700',
					text: 'true'
				});
			});
			layui.use('rate', function() {
				var rate = layui.rate;

				//渲染
				var ins1 = rate.render({
					elem: '#test2', //绑定元素
					theme: '#ff6700',
					text: 'true'
				});
			});
			layui.use('rate', function() {
				var rate = layui.rate;

				//渲染
				var ins1 = rate.render({
					elem: '#test3', //绑定元素
					theme: '#ff6700',
					text: 'true'
				});
			});
			layui.use('rate', function() {
				var rate = layui.rate;

				//渲染
				var ins1 = rate.render({
					elem: '#test4', //绑定元素
					theme: '#ff6700',
					text: 'true'
				});
			});
			layui.use('rate', function() {
				var rate = layui.rate;

				//渲染
				var ins1 = rate.render({
					elem: '#score2-1', //绑定元素
					theme: '#ff6700',
					text: 'true'
				});
			});
		},
		//		弹窗
		alert: function() {

			layui.use('layer', function() {
				var layer = layui.layer;
				$(".score4-2").mouseenter(function() {
					layer.open({
						type: 1,
						content: $('#tan'), //这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
						shadeClose: true,
						time: 1000,
					});
				});
			})

			layui.use('upload', function() {
				var upload = layui.upload;

				//执行实例
				var uploadInst = upload.render({
					elem: '.score4-1' //绑定元素
						,
					url: '/upload/' //上传接口
						,
					done: function(res) {
						//上传完毕回调
					},
					error: function() {
						//请求异常回调
					}
				});
			});
		}
	})
})(jQuery)