(function($) {
	$.extend({
		atlas: {}
	})
	$.extend($.atlas, {
		init: function() {
			this.lunboE()
			this.lunboF()
			this.lunboG()
			this.lunboH()
		},

		lunboE: function() {
			var indexE = 0

			function moveE() {
				timerE = setInterval(function() {
					var outIndexE = indexE
					indexE++
					if(indexE>($(".lunboE li").length - 1)) indexE = 0
					$(".lunboE li").eq(outIndexE).animate({
						"opacity": 0
					}, 500)
					$(".lunboE li").eq(indexE).animate({
						"opacity": 1
					}, 500);

					$(".squareE li").eq(indexE).addClass("activeE").siblings().removeClass("activeE")
				}, 2500)
			}
			moveE()

			function goE(transIndexE) {
				var outIndexE = indexE
				indexE = transIndexE
				if(indexE > ($(".lunboE li").length - 1)) {
					indexE = 0
				}
				$(".lunboE li").eq(outIndexE).animate({
					"opacity": 0
				}, 500)
				$(".lunboE li").eq(indexE).animate({
					"opacity": 1
				}, 500);

				$(".squareE li").eq(indexE).addClass("activeE").siblings().removeClass("activeE")
			}
			$(".squareE li").click(function() {
				clearInterval(timerE)
				goE($(".squareE li").index(this))
				setTimeout(function() {
					moveE()
				}, 0)
			})
			//向右点击
			$(".nextE").click(function() {
				clearInterval(timerE)
				var transordE = indexE + 1
				transordE = transordE > $(".lunboE li").length - 1 ? 0 : transordE;
				goE(transordE)
				setTimeout(function() {
					moveE()
				}, 0)
			})
			//向左点击
			$(".preE").click(function() {
				clearInterval(timerE)
				var transordE = indexE - 1
				transordE = transordE < 0 ? ($(".lunboE li").length - 1) : transordE;
				goE(transordE)
				setTimeout(function() {
					moveE()
				}, 0)
			})
		},

		lunboF: function() {
			var indexF = 0

			function moveF() {
				timerF = setInterval(function() {
					var outIndexF = indexF
					indexF++
					if(indexF > ($(".lunboF li").length - 1)) indexF = 0
					$(".lunboF li").eq(outIndexF).animate({
						"opacity": 0
					}, 500)
					$(".lunboF li").eq(indexF).animate({
						"opacity": 1
					}, 500);

					$(".squareF li").eq(indexF).addClass("activeF").siblings().removeClass("activeF")
				}, 2500)
			}
			moveF()

			function goF(transIndexF) {
				var outIndexF = indexF
				indexF = transIndexF
				if(indexF > ($(".lunboF li").length - 1)) {
					indexF = 0
				}
				$(".lunboF li").eq(outIndexF).animate({
					"opacity": 0
				}, 500)
				$(".lunboF li").eq(indexF).animate({
					"opacity": 1
				}, 500);

				$(".squareF li").eq(indexF).addClass("activeF").siblings().removeClass("activeF")
			}
			$(".squareF li").click(function() {
				clearInterval(timerF)
				goF($(".squareF li").index(this))
				setTimeout(function() {
					moveF()
				}, 0)
			})
			//向右点击
			$(".nextF").click(function() {
				clearInterval(timerF)
				var transordF = indexF + 1
				transordF = transordF > $(".lunboF li").length - 1 ? 0 : transordF;
				goF(transordF)
				setTimeout(function() {
					moveF()
				}, 0)
			})
			//向左点击
			$(".preF").click(function() {
				clearInterval(timerF)
				var transordF = indexF - 1
				transordF = transordF < 0 ? ($(".lunboF li").length - 1) : transordF;
				goF(transordF)
				setTimeout(function() {
					moveF()
				}, 0)
			})
		},

		lunboG: function() {
			var indexG = 0

			function moveG() {
				timerG = setInterval(function() {
					var outIndexG = indexG
					indexG++
					if(indexG > ($(".lunboG li").length - 1)) indexG = 0
					$(".lunboG li").eq(outIndexG).animate({
						"opacity": 0
					}, 500)
					$(".lunboG li").eq(indexG).animate({
						"opacity": 1
					}, 500);
					$(".squareG li").eq(indexG).addClass("activeG").siblings().removeClass("activeG")
				}, 2500)
			}
			moveG()

			function goG(transIndexG) {
				var outIndexG = indexG
				indexG = transIndexG
				if(indexG > ($(".lunboG li").length - 1)) {
					indexG = 0
				}
				$(".lunboG li").eq(outIndexG).animate({
					"opacity": 0
				}, 500)
				$(".lunboG li").eq(indexG).animate({
					"opacity": 1
				}, 500);
				$(".squareG li").eq(indexG).addClass("activeG").siblings().removeClass("activeG")
			}
			$(".squareG li").click(function() {
				clearInterval(timerG)
				goG($(".squareG li").index(this))
				setTimeout(function() {
					moveG()
				}, 0)
			})
			//向右点击
			$(".nextG").click(function() {
				clearInterval(timerG)
				var transordG = indexG + 1
				transordG = transordG > $(".lunboG li").length - 1 ? 0 : transordG;
				goG(transordG)
				setTimeout(function() {
					moveG()
				}, 0)
			})
			//向左点击
			$(".preG").click(function() {
				clearInterval(timerG)
				var transordG = indexG - 1
				transordG = transordG < 0 ? $(".lunboG li").length - 1 : transordG;
				goG(transordG)
				setTimeout(function() {
					moveG()
				}, 0)
			})
		},
		
		lunboH: function() {
			var indexH = 0

			function moveH() {
				timerH = setInterval(function() {
					var outIndexH = indexH
					indexH++
					if(indexH > ($(".lunboH li").length - 1)) indexH = 0
					$(".lunboH li").eq(outIndexH).animate({
						"opacity": 0
					}, 500)
					$(".lunboH li").eq(indexH).animate({
						"opacity": 1
					}, 500);
					$(".squareH li").eq(indexH).addClass("activeH").siblings().removeClass("activeH")
				}, 2500)
			}
			moveH()

			function goH(transIndexH) {
				var outIndexH = indexH
				indexH = transIndexH
				if(indexH > ($(".lunboH li").length - 1)) {
					indexH = 0
				}
				$(".lunboH li").eq(outIndexH).animate({
					"opacity": 0
				}, 500)
				$(".lunboH li").eq(indexH).animate({
					"opacity": 1
				}, 500);
				$(".squareH li").eq(indexH).addClass("activeH").siblings().removeClass("activeH")
			}
			$(".squareH li").click(function() {
				clearInterval(timerH)
				goH($(".squareH li").index(this))
				setTimeout(function() {
					moveH()
				}, 0)
			})
			//向右点击
			$(".nextH").click(function() {
				clearInterval(timerH)
				var transordH = indexH + 1
				transordH = transordH > $(".lunboH li").length - 1 ? 0 : transordH;
				goH(transordH)
				setTimeout(function() {
					moveH()
				}, 0)
			})
			//向左点击
			$(".preH").click(function() {
				clearInterval(timerH)
				var transordH = indexH - 1
				transordH = transordH < 0 ? $(".lunboH li").length - 1 : transordH;
				goH(transordH)
				setTimeout(function() {
					moveH()
				}, 0)
			})
		},

	});
})(jQuery)