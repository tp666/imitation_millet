(function($) {
	$.extend({
		summary: {}
	})
	$.extend($.summary, {
		init: function() {
			this.lunboA()
			this.lunboB()
			this.lunboD()
			this.loadCemeraList()
			this.loadVideoList()
			this.loadAIcemeraList()
			this.loadduotulunbo()
		},
		
		

		lunboA: function() {
			var indexA = 0
			//自动播放
			function moveA() {
				timerA = setInterval(function() {
					//记录当前序号
					var outIndexA = indexA
					//改变要显示图片的序号（淡出图片序号 加一）
					indexA++
					if(indexA > ($(".lunboA li").length - 1)) indexA = 0
					$(".lunboA li").eq(outIndexA).animate({
						"opacity": 0
					}, 500);
					$(".lunboA li").eq(indexA).animate({
						"opacity": 1
					}, 500);
					$(".square li").eq(indexA).addClass("activeA").siblings().removeClass("activeA")
				}, 2500)
			}
			moveA()
			//跳转到指定图片
			function goA(transIndexA) {
				var outIndexA = indexA
				indexA = transIndexA
				if(indexA > ($(".lunboA li").length - 1)) {
					indexA = 0
				}
				$(".lunboA li").eq(outIndexA).animate({
					"opacity": 0
				}, 500);
				$(".lunboA li").eq(indexA).animate({
					"opacity": 1
				}, 500);
				$(".square li").eq(indexA).addClass("activeA").siblings().removeClass("activeA")
			}
			//点击下方矩形跳转至相应图片
			$(".square li").click(function() {
				clearInterval(timerA)
				goA($(".square li").index(this))
				setTimeout(function() {
					moveA()
				}, 0)
			})
		},

		lunboB: function() {
			var indexB = 0

			function moveB() {
				timerB = setInterval(function() {
					var outIndexB = indexB
					indexB++
					if(indexB > ($(".lunboB li").length - 1)) indexB = 0
					$(".lunboB li").eq(outIndexB).animate({
						"opacity": 0
					}, 500)
					$(".lunboB li").eq(indexB).animate({
						"opacity": 1
					}, 500);
					$(".squareB li").eq(indexB).addClass("activeB").siblings().removeClass("activeB")
				}, 2500)
			}
			moveB()

			function goB(transIndexB) {
				var outIndexB = indexB
				indexB = transIndexB
				if(indexB > ($(".lunboB li").length - 1)) {
					indexB = 0
				}
				$(".lunboB li").eq(outIndexB).animate({
					"opacity": 0
				}, 500)
				$(".lunboB li").eq(indexB).animate({
					"opacity": 1
				}, 500);
				$(".squareB li").eq(indexB).addClass("activeB").siblings().removeClass("activeB")
			}
			$(".squareB li").click(function() {
				clearInterval(timerB)
				goB($(".squareB li").index(this))
				setTimeout(function() {
					moveB()
				}, 0)
			})
			//向右点击
			$(".next").click(function() {
				clearInterval(timerB)
				var transord = indexB + 1
				transord = transord > $(".lunboB li").length - 1 ? 0 : transord;
				goB(transord)
				setTimeout(function() {
					moveB()
				}, 0)
			})
			//向左点击
			$(".pre").click(function() {
				clearInterval(timerB)
				var transord = indexB - 1
				transord = transord < 0 ? $(".lunboB li").length - 1 : transord;
				goB(transord)
				setTimeout(function() {
					moveB()
				}, 0)
			})
		},

		loadCemeraList: function() {
			$.get("data/summary.cemera.json", function(data) {
				data.forEach(function(item) {
					$(".cemerapic").append(`<li class="item">
					<div class="cemerapicA" style="background: url(img/summary/${item.img}) no-repeat center center;"></div>
					<div class="cemerapicB">${item.name}</div>
				</li>`)
				})
			})
		},

		lunboD: function() {
			var indexD = 0

			function moveD() {
				timerD = setInterval(function() {
					var outIndexD = indexD
					indexD++
					if(indexD > ($(".lunboD li").length - 1)) indexD = 0
					$(".lunboD li").eq(outIndexD).animate({
						"opacity": 0
					}, 500)
					$(".lunboD li").eq(indexD).animate({
						"opacity": 1
					}, 500);
					$(".lbDwd li").eq(outIndexD).animate({
						"opacity": 0
					}, 500)
					$(".lbDwd li").eq(indexD).animate({
						"opacity": 1
					}, 500);
					$(".squareD li").eq(indexD).addClass("activeD").siblings().removeClass("activeD")
				}, 2500)
			}
			moveD()

			function goD(transIndexD) {
				var outIndexD = indexD
				indexD = transIndexD
				if(indexD > ($(".lunboD li").length - 1)) {
					indexD = 0
				}
				$(".lunboD li").eq(outIndexD).animate({
					"opacity": 0
				}, 500)
				$(".lunboD li").eq(indexD).animate({
					"opacity": 1
				}, 500);
				$(".lbDwd li").eq(outIndexD).animate({
					"opacity": 0
				}, 500)
				$(".lbDwd li").eq(indexD).animate({
					"opacity": 1
				}, 500);
				$(".squareD li").eq(indexD).addClass("activeD").siblings().removeClass("activeD")
			}
			$(".squareD li").click(function() {
				clearInterval(timerD)
				goD($(".squareD li").index(this))
				setTimeout(function() {
					moveD()
				}, 0)
			})
			//向右点击
			$(".nextD").click(function() {
				clearInterval(timerD)
				var transordD = indexD + 1
				transordD = transordD > $(".lunboD li").length - 1 ? 0 : transordD;
				goD(transordD)
				setTimeout(function() {
					moveD()
				}, 0)
			})
			//向左点击
			$(".preD").click(function() {
				clearInterval(timerD)
				var transordD = indexD - 1
				transordD = transordD < 0 ? ($(".lunboD li").length - 1) : transordD;
				goD(transordD)
				setTimeout(function() {
					moveD()
				}, 0)
			})
		},
		loadVideoList: function() {
			$.get("data/summary.video.json", function(data) {
				data.forEach(function(item) {
					$(".handsmall").append(`<li>
					<div class="small" style="background: url(img/summary/${item.img}) no-repeat center center;"></div>
					<div class="smallwd">${item.name}</div>
				</li>`)
				})
			})
		},

		loadAIcemeraList: function() {
			$.get("data/summary.aicemera.json", function(data) {
				data.forEach(function(item) {
					$(".aicemera").append(`<li>
				<div class="aicemerapic" style="background: url(img/summary/${item.img}) 100% 100% ;background-size: cover;">					
				</div>
				<div class="aicemerawd">
					<p style="font-size: 16px;">${item.name1}</p>
					<p style="font-size: 14px;">${item.name2}</p>
				</div>
			</li>`)
				})
			})
		},

		loadduotulunbo: function() {
			var swiper = new Swiper('.swiper-container', {
				//图片间距
				spaceBetween: 15,
				//整屏幕共几张图片
				slidesPerView: 2,
				//中间图居中
				centeredSlides: true,
				//点图片上一张或者下一张，鼠标拖动上一张下一张
				slideToClickedSlide: true,
				//鼠标放上去变图标
				//grabCursor: true,
				nextButton: '.swiper-button-next',
				prevButton: '.swiper-button-prev',
				//scrollbar: '.swiper-scrollbar',
				pagination: '.swiper-pagination',
				history: 'slide',
				//循环
				loop: true,
				//autoplay: 3000,
				//speed: 300,
			});
		}

	});
})(jQuery)