(function($) {

	$.fn.extend({
		initPager: function(totalcount, pagesize, jump) {

			var pagecount = Math.floor(totalcount / pagesize)

			if(totalcount % pagesize !== 0) {
				pagecount++
			}

			var obj = $(this)
			obj.append(`<ul class="pager"></ul>`)

			var currentPageIndex = 1

			//判断禁用
			function checkDisabled() {
				var start = (currentPageIndex - 1) * pagesize
				var end = start + pagesize

				if(jump) jump(currentPageIndex, start, end)

				//先将按钮状态都还原
				obj.find(".disbaled").removeClass("disbaled")
					//再根据页码判断是否需要禁用按钮
				if(currentPageIndex === 1) {
					obj.find(".pager li.ctr.first").addClass("disbaled")
					obj.find(".pager li.ctr.prev").addClass("disbaled")
				} else if(pagecount === currentPageIndex) {
					obj.find(".pager li.ctr.last").addClass("disbaled")
					obj.find(".pager li.ctr.next").addClass("disbaled")
				}
			}

			//创建分页
			function createPager() {
				obj.find(".pager").append(`
				<li class="ctr first">首页</li>
			<li class="ctr prev">上页</li>
			`)
					//生成页码
				for(var i = 1; i <= pagecount; i++) {
					obj.find(".pager").append(`<li class = "item" >${i}</li>`)
				}

				obj.find(".pager").append(`<li class="ctr next">下页</li>
			<li class="ctr last">尾页</li>`)
					//设置页码1高亮
				obj.find(".pager li.item").eq(currentPageIndex - 1).addClass("active")

				//检查是否需要禁用
				checkDisabled()
			}

			createPager()

			//点击具体页码
			$(this).find(".pager li.item").click(function() {
				//修改当前页码
				currentPageIndex = parseInt($(this).text())
					//检查是否需要禁用
				checkDisabled()
					//切换样式
				$(this).addClass("active").siblings(".item").removeClass("active")
			})

			//点击下页
			obj.find(".pager li.ctr.next").click(function() {
				//到头了
				if(currentPageIndex === pagecount) return
					//页码++
				currentPageIndex++
				obj.find(".pager li.item").eq(currentPageIndex - 1).addClass("active").siblings(".item").removeClass("active")
					//检查是否需要禁用
				checkDisabled()
			})

			//点击上页
			obj.find(".pager li.ctr.prev").click(function() {
				//到头了
				if(currentPageIndex === 1) return
				currentPageIndex--
				obj.find(".pager li.item").eq(currentPageIndex - 1).addClass("active").siblings(".item").removeClass("active")
					//检查是否需要禁用
				checkDisabled()
			})

			//点击首页
			obj.find(".pager li.ctr.first").click(function() {
				currentPageIndex = 1
				obj.find(".pager li.item").eq(currentPageIndex - 1).addClass("active").siblings(".item").removeClass("active")
					//检查是否需要禁用
				checkDisabled()
			})

			//点击尾页
			obj.find(".pager li.ctr.last").click(function() {
				currentPageIndex = pagecount
				obj.find(".pager li.item").eq(currentPageIndex - 1).addClass("active").siblings(".item").removeClass("active")
					//检查是否需要禁用
				checkDisabled()
			})
		}
	})

})(jQuery)