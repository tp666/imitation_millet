var headerstr = `<div class="doahang">
			<div class="daohang1">
				<div class="daohang2">
					<a class="topname">小米商城</a>
					<a class="topname1">|</a>
					<a class="topname">MIUI</a>
					<a class="topname1">|</a>
					<a class="topname">IoT</a>
					<a class="topname1">|</a>
					<a class="topname">云服务</a>
					<a class="topname1">|</a>
					<a class="topname">金融</a>
					<a class="topname1">|</a>
					<a class="topname">有品</a>
					<a class="topname1">|</a>
					<a class="topname">小爱开放平台</a>
					<a class="topname1">|</a>
					<a class="topname">政企服务</a>
					<a class="topname1">|</a>
					<a class="topname">Select Region</a>
				</div>
				<div class="daohang4">
				    <i class="fa fa-cart-plus fa-lg daohang4-fa" aria-hidden="true"></i>
					<a class="ziti">购物车（<i class="shopcar-count">0</i>）</a>
					<div class="gouwu">
						购物车中还没有商品，赶紧选购吧！
					</div>	
				</div>
				<div class="daohang3">
					<a class="topname denglu">登录</a>
					<a class="topname1">|</a>
					<a class="topname tuichu">注册</a>
					<a class="topname1">|</a>
					<a class="topname">消息通知</a>
				</div>
			</div>
		</div>
		<div style="width: 100%;">
			<div class="logo">
				<div class="logo_" style="cursor: pointer"></div>
				<div class="logo1">
					<div class="allgoods">全部商品分类
						<div class="banner-dao">
							<ul class="banner2">
							</ul>
							<ul class="banner3">
								<li style="	width: 990px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 990px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 495px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 742.5px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 495px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 247.5px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 742.5px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 742.5px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 247.5px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
								<li style="	width: 990px;height: 458px;">
									<ul class="banner4"></ul>
								</li>
							</ul>
						</div>
					</div>

					<ul class="logoRight">
						<li class="it">小米手机</li>
						<li class="it">红米</li>
						<li class="it">电视</li>
						<li class="it">笔记本</li>
						<li class="it">空调</li>
						<li class="it">新品</li>
						<li class="it">路由器</li>
						<li class="it">智能硬件</li>
						<li>服务</li>
						<li>社区</li>
					</ul>
					<div class="logoxia">
						<ul class="logowu">
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/小米1.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米MIX 3</div>
										<div class="xinghao" style="color:#ff6700 ;">3299元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/小米2.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8 青春版</div>
										<div class="xinghao" style="color:#ff6700 ;">1399元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/小米3.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8 屏幕指纹版</div>
										<div class="xinghao" style="color:#ff6700 ;">3199元起</div>
									</li>

									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/小米4.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8</div>
										<div class="xinghao" style="color:#ff6700 ;">2699元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/小米5.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8 SE</div>
										<div class="xinghao" style="color:#ff6700 ;">1799元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/小米6.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米Max 3</div>
										<div class="xinghao" style="color:#ff6700 ;">1699元起</div>
									</li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/红米1.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">红米6 Pro</div>
										<div class="xinghao" style="color:#ff6700 ;">999元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/红米2.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">红米6</div>
										<div class="xinghao" style="color:#ff6700 ;">799元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/红米3.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">红米6A</div>
										<div class="xinghao" style="color:#ff6700 ;">599元</div>
									</li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/红米4.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">红米Note 5</div>
										<div class="xinghao" style="color:#ff6700 ;">1099元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/红米5.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">红米S2</div>
										<div class="xinghao" style="color:#ff6700 ;">999元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/电视1.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米电视4C 40英寸</div>
										<div class="xinghao" style="color:#ff6700 ;">1199元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/电视2.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米电视4A 32英寸</div>
										<div class="xinghao" style="color:#ff6700 ;">849元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/电视3.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米电视4A 43英寸青春版</div>
										<div class="xinghao" style="color:#ff6700 ;">1399元</div>
									</li>

									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/电视4.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米电视4A 50英寸</div>
										<div class="xinghao" style="color:#ff6700 ;">1899元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/电视5.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米电视4A 55英寸</div>
										<div class="xinghao" style="color:#ff6700 ;">2299元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/电视6.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">查看全部</div>
										<div class="xinghao" style="color:#ff6700 ;">小米电视</div>
									</li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/电脑1.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米笔记本 Air 12.5"</div>
										<div class="xinghao" style="color:#ff6700 ;">3599起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/电脑2.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米笔记本 Air 13.3"</div>
										<div class="xinghao" style="color:#ff6700 ;">4999元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/电脑3.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米笔记本 15.6"</div>
										<div class="xinghao" style="color:#ff6700 ;">4199元起</div>
									</li>

									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/电脑4.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米笔记本 Pro 15.6"</div>
										<div class="xinghao" style="color:#ff6700 ;">5599元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/电脑5.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米游戏本</div>
										<div class="xinghao" style="color:#ff6700 ;">6699元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/电脑6.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米平板</div>
										<div class="xinghao" style="color:#ff6700 ;">1099元起</div>
									</li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/空调1.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">米家互联网空调</div>
										<div class="xinghao" style="color:#ff6700 ;">预售到手价1999元起</div>
									</li>

								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/新品1.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8 青春版</div>
										<div class="xinghao" style="color:#ff6700 ;">1399元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/新品2.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米8 屏幕指纹版</div>
										<div class="xinghao" style="color:#ff6700 ;">3199元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/新品3.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米手环3 NFC版</div>
										<div class="xinghao" style="color:#ff6700 ;">199元</div>
									</li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/新品4.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米米家小白只能摄像机</div>
										<div class="xinghao" style="color:#ff6700 ;">399元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/新品5.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米小爱只能闹钟</div>
										<div class="xinghao" style="color:#ff6700 ;">149元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/新品6.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米双单元半入耳式耳机</div>
										<div class="xinghao" style="color:#ff6700 ;">99元</div>
									</li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/路由器1.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米路由器4</div>
										<div class="xinghao" style="color:#ff6700 ;">179元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/路由器2.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米路由器 4Q</div>
										<div class="xinghao" style="color:#ff6700 ;">89元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">新品</div>
										<img class="pic" src="img/header/路由器3.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米路由器 3A</div>
										<div class="xinghao" style="color:#ff6700 ;">109元</div>
									</li>

									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/路由器4.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米MIX 3</div>
										<div class="xinghao" style="color:#ff6700 ;">3299元起</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping">热卖</div>
										<img class="pic" src="img/header/路由器5.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米路由器 3G</div>
										<div class="xinghao" style="color:#ff6700 ;">219元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/路由器6.png" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米路由器HD/Pro</div>
										<div class="xinghao" style="color:#ff6700 ;">499元起</div>
									</li>
								</ul>
							</li>
							<li>
								<ul class="logowuping">
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能1.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">米家空气净化器Pro</div>
										<div class="xinghao" style="color:#ff6700 ;">1499元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能2.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">小米净水器</div>
										<div class="xinghao" style="color:#ff6700 ;">1999元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能3.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">米家扫地机器人</div>
										<div class="xinghao" style="color:#ff6700 ;">1699元</div>
									</li>

									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能4.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">米家压力IH电饭煲</div>
										<div class="xinghao" style="color:#ff6700 ;">999元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能5.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">米家电动剃须刀</div>
										<div class="xinghao" style="color:#ff6700 ;">199元</div>
									</li>
									<li style="height: 110px;width: 1px;background: #E0E0E0;"></li>
									<li>
										<div class="xinping1"></div>
										<img class="pic" src="img/header/智能6.jpg" style="height: 100%;width: 100%;" />
										<div class="xinghao">查看全部</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>

				</div>
				<div class="logoshou">
					<input type="text" id="keywords" class="logoshou1" />
					<div class="aa" >
						<i class="fa fa-search fa-lg aa-fa" aria-hidden="true"></i>
					</div>
				</div>
			</div>
		</div>
		`
$("header")[0].innerHTML = headerstr